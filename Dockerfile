FROM python:3.9

WORKDIR /code

COPY requirements.txt /code/

RUN pip install --upgrade pip
RUN pip install -r requirements.txt --no-cache-dir

COPY . /code/

CMD ["gunicorn", "challenge1.wsgi", ":8000"]